import requests
import sys

file_name = sys.argv[1]
scan_type = ''

if file_name == 'gitleaks.json':
    scan_type = 'Gitleaks Scan'
elif file_name == 'njsscan.sarif':
    scan_type = 'SARIF'
elif file_name == 'semgrep.json':
    scan_type = 'Semgrep JSON Report'
elif file_name == 'retire.json':
    scan_type = 'Retire.js Scan'


headers = {
    'Authorization': 'Token 2e5a9bc18adb55ed18e2c40cf765cfcb98b29525'
}

url = 'https://demo.defectdojo.org/api/v2/import-scan/'

data = {
    
    'active': True,
    'verfied': True,
    'scan_type': scan_type,
     'engagement': 14
}

files = {
    'file' : open(file_name, 'rb')
}

response= requests.post(url, headers=headers, data=data, files=files)

if response.status_code == 201:
    print('Scan resutls imported seccessfully')
else:
    print(f'Failed to import scan results {response.content}')